FROM node:alpine

RUN mkdir -p /usr/src/app
COPY server /usr/src/app

WORKDIR /usr/src/app

RUN npm install

EXPOSE 8888

ENTRYPOINT ["npm","run","prod"]
