# Presentation

This repository holds the result of a technical test for earth cube.  
There is a directory ui with the react and a directory server with the code serving the datas and the ui in production environment.  

# Usage

You can use the launch.sh script which will build the ui, create the docker and launch it. Then, you just have to go to http://localhost:8080.  
Be sure you have node js version >= 8 installed and that nothing is runnning on 8080.
