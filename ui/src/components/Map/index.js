import React from 'react';
import ReactMapGL, {Marker, Popup} from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import * as R from 'ramda';
import {keyToTitle} from '../../helpers/transformers';

const TOKEN = 'pk.eyJ1IjoidGhlc3Bva2UiLCJhIjoiY2oxYzRveHVrMDA1MTMzcDR0Z2JmYmhkcyJ9.bN-VFhlhBrQ2iIHdT9Lkeg';
const ORIGIN = [48.852966, 2.349902];
const OFFSET = .03

const changeArrayElement = (array, index, element) =>
  [].concat(...array.slice(0, index), element, ...array.slice(index + 1, array.length))

class AppMap extends React.Component {
  state = {
    points: null,
    viewport: {
      width: 1280,
      height: 960,
      latitude: ORIGIN[0],
      longitude: ORIGIN[1],
      zoom: 12,
    }
  };

  componentWillMount() {
    const points = this.createPointsFromData(this.props.data);
    this.setState({
      ...this.state,
      points,
    });
  }

  componentWillReceiveProps(nextProps) {
    const points = this.createPointsFromData(this.props.data);
    this.setState({
      ...this.state,
      points,
    });
  }

  createPointsFromData = ({dates, datas}) => datas
    .map((d) => ({
        coordinates: ORIGIN.map(p => (p + Math.random() * OFFSET * ((Math.random() > .5) ? -1 : 1))),
        properties: d,
        isOpen: false,
      }))

  createMarker = ({properties, coordinates}, index) =>
    <Marker
      key={properties.name}
      latitude={coordinates[0]}
      longitude={coordinates[1]}>

      <img
        alt='position-circle'
        style={{height: 24, width: 24}}
        src="position-circle.svg"
        onClick={() => this.setState({
          ...this.state,
          points: changeArrayElement(this.state.points, index, {coordinates, properties, isOpen: true}),
        })} />

    </Marker>

  renderPersonProperty = (dates) => ([key, value]) =>
    <div key={key}>
      <span style={{fontWeight: 'bold'}}>{keyToTitle(key)}:</span>
      <span> {
        !Array.isArray(value) ?
        value :
        <ul>
          {value.map((v, i) =>
            <li key={i}>
              {dates[i]}: {v || '<no info>'}
            </li>)}
        </ul>}
      </span>
    </div>

  createPopup = (dates) => ({isOpen, properties, coordinates}, index) =>
    isOpen &&
      <Popup
        key={properties.name}
        latitude={coordinates[0]}
        longitude={coordinates[1]}
        closeButton={true}
        onClose={() => this.setState({
          ...this.state,
          points: changeArrayElement(this.state.points, index, {coordinates, properties, isOpen: false})
        })}>

        <div>
          {R.toPairs(properties).map(this.renderPersonProperty(dates))}
        </div>

      </Popup>;

  render() {
    const {data: {dates}} = this.props;
    const {viewport, points} = this.state;

    return (
      <div style={{textAlign: 'justify'}}>
        <ReactMapGL
          {...viewport}
          mapStyle='mapbox://styles/mapbox/basic-v9'
          mapboxApiAccessToken={TOKEN}
          onViewportChange={(viewport) => this.setState({...this.state, viewport})}>
        {points.map(this.createMarker)}
        {points.map(this.createPopup(dates))}
        </ReactMapGL>
      </div>
    );
  }
}

export default AppMap;
