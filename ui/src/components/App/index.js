import React, {Component} from 'react';
import {Alert, Layout, Menu, Spin} from 'antd';
import AppMap from '../Map';
import Graph from '../Graph';
import Table from '../Table';
import './App.css';

const toTitle = (str) => str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()

const requestData = () => (
  process.env.NODE_ENV === 'development' ?
  fetch('http://localhost:8888/api/data', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'cors',
  }) :
  fetch('/api/data', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
);

const getData = () => requestData().then((response) => response.json());

class App extends Component {
  static menuItems = ['table', 'map', 'chart'];
  state = {
    selectedKey: 'table',
    data: null,
  };

  componentWillMount() {
    getData()
    .then((data) => this.setState({
      ...this.state,
      data,
    }))
    .catch((err) => this.setState({
      ...this.state,
      data: new Error('Unable to obtain the data from the server'),
    }));
  }

  handleSelectKeys = ({selectedKeys}) => this.setState({
    ...this.state,
    selectedKey: selectedKeys[0],
  })

  renderContent = () => {
    const {data, selectedKey} = this.state;

    if (data === null) {
      return <Spin />;
    }
    if (data instanceof Error) {
      const padding = 24;
      return (
        <div style={{paddingLeft: padding, paddingRight: padding}}>
          <Alert type='error' description={data.toString()} />
        </div>
      );
    }

    switch (selectedKey) {
      case 'chart':
        return <Graph data={data} />;
      case 'map':
        return <AppMap data={data} />;
      default:
        return <Table data={data} />;
    }
  }

  render() {
    const {selectedKey} = this.state;
    const titledKey = toTitle(selectedKey);

    return (
      <Layout style={{minHeight: '100vh'}}>
        <Layout.Header>
          <Menu
            mode='horizontal'
            theme='dark'
            selectedKeys={[selectedKey]}
            onSelect={this.handleSelectKeys}
            style={{lineHeight: '64px'}}>
            <Menu.Item disabled style={{cursor: 'default'}}>
              <div>
                <img alt='earth-cube-logo' style={{height: 32}} src="earth-cube-logo.png" />
                <span>Technical test @ EarthCube</span>
              </div>
            </Menu.Item>
            {App.menuItems.map(item =>
                <Menu.Item key={item}>{item.charAt(0).toUpperCase() + item.slice(1)}</Menu.Item>)}
          </Menu>
        </Layout.Header>

        <Layout.Content
          style={{
            padding: 24,
          }}>
          <div style={{padding: 12}}>{titledKey}</div>
          <div style={{textAlign: 'center'}}>
            <div style={{padding: 48, background: 'white'}}>
              {this.renderContent()}
            </div>
          </div>
        </Layout.Content>

        <Layout.Footer style={{textAlign: 'center'}}>
          Done by Jules Faucherre
        </Layout.Footer>
      </Layout>
    );
  }
}

export default App;
