import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  PieChart,
  Pie,
  Cell,
} from 'recharts';
import * as R from 'ramda';
import * as randomColor from 'random-color';

const invert = (matrix) => {
  const base = R.range(0, matrix[0].length).map(() => []);
  return matrix.reduce((acc, array) => acc.map((line, index) => line.concat(array[index])), base);
}

class Graph extends React.Component {
  static createColorSelectorFromValues = (values) =>
    values.reduce((acc, v) => Object.assign({}, acc, {[v]: randomColor(0.60, 0.90).hexString()}), {});

  static createBarSerie = (dates, datas) => datas.map(({name, kmPerYear}) => ({
      name,
      ...kmPerYear.reduce((acc, v, i) => ({
        ...acc,
        [dates[i]]: v && Number(v.split(/\s+/).join('')),
      }), {})
    }));

  static createPieSerie = R.compose(
    R.map(R.compose(
      R.map(([name, value]) => ({name, value})),
      R.toPairs,
      R.countBy(R.identity),
      R.filter(v => v !== null && v !== 'NA')
    )),
    invert,
    R.map(R.prop('carModel'))
  );

  renderBarChart = () => {
    const {data: {dates, datas}} = this.props;
    const series = Graph.createBarSerie(dates, datas);
    const barColors = Graph.createColorSelectorFromValues(dates);

    return (
      <BarChart
        data={series}
        width={960}
        height={800}
        margin={{top: 20, right: 30, left: 20, bottom: 5}}>

        <XAxis dataKey='name' />
        <YAxis />
        <CartesianGrid />
        <Tooltip />
        <Legend />
        {dates.map((date, index) =>
          <Bar key={date} dataKey={date} fill={barColors[date]} />)}

      </BarChart>
    );
  }

  renderPieLegend = (dates, carToColors) => (
    <ul type='none'>
      {R.toPairs(carToColors).map(([car, color], index) =>
        <li key={index}>
          <span style={{color}}>&#9632; </span>
          <span>{car}</span>
        </li>)}
    </ul>
  );

  static extractCarModels = R.compose(
    // Only keep unique values
    R.uniq,
    // Lowercase the values
    R.map(R.toLower),
    // Flatten the array
    R.flatten,
    // Construct array of array of car name
    R.map(R.map(R.prop('name'))),
  );

  renderPieChart = () => {
    const {data: {dates, datas}} = this.props;
    const series = Graph.createPieSerie(datas);
    const size = series.length * 200;
    const carModels = Graph.extractCarModels(series);
    const carToColors = Graph.createColorSelectorFromValues(carModels);

    return (
      <React.Fragment>
        <PieChart
          width={size}
          height={size}
          dataKey='name'
          margin={{top: 20, right: 30, left: 30, bottom: 5}}>
          {series.map((serie, index) =>
            <Pie
              dataKey='value'
              key={dates[index]}
              cx={size / 2} cy={size / 2}
              outerRadius={(index + 1) * 80}
              innerRadius={(index + 1) * 70}
              data={serie} label>
            {serie.map((d, index) => <Cell key={index} fill={carToColors[d.name.toLowerCase()]} />)}
            </Pie>)}
        </PieChart>

        {this.renderPieLegend(dates, carToColors)}

        <div>
          The circles represent from the inner to the outer
          <ul>
            {dates.map((date) =>
              <li key={date}>{date}</li>
            )}
          </ul>
        </div>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div style={{
        textAlign: 'left',
      }}>

        <div>Kilometers/Year Per people</div>
        {this.renderBarChart()}

        <div style={{marginTop: 48}}>Car Models Per Year</div>
        {this.renderPieChart()}

      </div>
    );
  }
}

export default Graph;
