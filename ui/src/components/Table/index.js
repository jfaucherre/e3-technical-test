import React from 'react';
import {Table as AntdTable} from 'antd';
import * as R from 'ramda';
import uuid from 'uuid';
import {keyToTitle} from '../../helpers/transformers';

class Table extends React.Component {
  state = {
    sortKey: null,
    sortOrder: 'ascend',
  };

  handleChange = (pagination, filters, {order, field}) => this.setState({
    sortKey: field,
    sortOrder: order,
  });

  render() {
    const {data: {dates, datas}} = this.props;
    const columns = R.toPairs(datas[0])
      .map(([key, value]) => {
        if (!Array.isArray(value)) {
          return {
            title: keyToTitle(key),
            dataIndex: key,
            key,
          };
        } else {
          return {
            title: keyToTitle(key),
            dataIndex: key,
            key,
            children: value
              .map((date, index) => ({
                title: dates[index],
                dataIndex: `${key}`,
                key: `${key}${index}`,
                render: (data) => data[index] || 'X',
              }))
          };
        }
      });

    return <AntdTable
      bordered
      rowKey={uuid}
      columns={columns}
      dataSource={datas}
      onChange={this.handleChange} />
  };
}

export default Table;
