const keyToTitleDict = {
  kmPerYear: 'Km / Year',
  name: 'Name',
  age: 'Age',
  carModel: 'Car Model',
};

export const keyToTitle = (key) => keyToTitleDict[key] || `Unknown key ${key}`;
