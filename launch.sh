#!/bin/sh
docker rm -f e3-technical-test
docker rmi -f e3-technical-test

npm install --prefix ui
npm run build --prefix ui

docker build -t e3-technical-test .
docker run -p 8080:8888 --name e3-technical-test -d e3-technical-test

echo "The site is served at http://localhost:8080"
