const path = require('path')
const express = require('express');
const favicon = require('serve-favicon');
const platformPath = path.resolve(__dirname, 'site.build');

module.exports = (app) => {
  app.use(favicon(path.join(platformPath, 'earth-cube-logo.png')));
  app.use(express.static(platformPath));
  app.get('*', (req, res) =>
    res.status(200).sendFile('index.html', {root: platformPath}));
};
