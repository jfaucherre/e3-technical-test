const cors = require('cors');
const express = require('express');
const {receiveData} = require('./formatData');
const winston = require('winston');
const morgan = require('morgan');
const serveSite = require('./serveSite');

const app = express();

app.use(morgan('tiny'));

if (process.env.NODE_ENV === 'development') {
  app.use(cors());
}

const router = express.Router();

router.get('/data', (req, res) =>
  receiveData('data').then((results) => res.json(results)));

app.use('/api', router);

if (process.env.NODE_ENV === 'production') {
  serveSite(app);
}

const port = 8888;
app.listen(port, () => console.log('Launched app on', port, '...'));
