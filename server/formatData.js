/*
 * The goal of this formatter file is to have better formatted data
 * The data is parsed in the following format
 *
 * {
 *  'filename = date': [{
 *    prénom, nom, age, modèle voiture, km/ans
 *  }]
 * }
 *
 * and aims at being output the following format:
 * {
 *   dates: [ISO formatted date],
 *   datas: [{
 *     name,
 *     age: [array of length of dates],
 *     carModel: [array of length of dates],
 *     kmPerYear: [array of length of dates],
 *   }]
 * }
 */

const fs = require('fs');
const path = require('path');
const csvtojson = require('csvtojson');
const R = require('ramda');
const moment = require('moment');

const debugMiddleware = (prefix) => (thing) => {console.log(prefix, thing); return thing}
const toTitle = (str) => str
  .split(' ')
  .map(s => s.charAt(0).toUpperCase() + s.slice(1).toLowerCase())
  .join(' ');

module.exports = {};

/*
 * Open data dir, parses all the csv files, format the datas as specified upper and returns
 */
const receiveData = (filename) => {
  const files = fs.readdirSync(filename);

  const result = files
    .map((file) => path.join('data', file))
    .filter((filename) => path.extname(filename) === '.csv' && !fs.statSync(filename).isDirectory())
    .map((filename, index) => new Promise((resolve) => {
      const data = [];
      const parser = csvtojson().fromFile(filename);
      parser.on('json', (entity) => data.push(entity));
      parser.on('end', () => resolve({
        filename: path.parse(filename).name,
        data,
      }));
    }));

  return Promise.all(result)
    .then(checkFormat)
    .then(formatData)
    //.then((results) => results.reduce((acc, {filename, data}) => Object.assign({}, {[filename]: data}, acc), {}))
    .catch(console.error);
};

const EXPECTED_OBJECT = {
  'prénom': /\w+/,
  'nom': /\w+/,
  'age': /\d+/,
  'modèle voiture': /[\w\s]+/,
  'km/ans': /[\d\s]+/,
};

/*
 * Check the format of the datas thanks to EXPECTED_OBJECT
 */
const checkFormat = datas => {
  datas.forEach(({filename, data}) => {
    if (!/\d{1,2}_\d{1,2}_\d{4}/.test(filename)) {
      throw new Error(`Filename ${filename} not date-formatted`);
    }
    data.forEach(d => {
      if (!R.compose(R.equals(R.keys(EXPECTED_OBJECT)), R.keys)) {
        throw new Error(`Object have keys: ${Object.keys(d)} but expected ${OBJECT_KEYS}`);
      }
      R.forEachObjIndexed((v, k) => {
        if (EXPECTED_OBJECT[k].test(v)) {
          throw new Error(`Object key ${k} is not well formatted`);
        }
      });
    });
  });
  return datas;
}

/*
 * Format the data as explained upper
 */
const formatData = (baseData) => {
  // Sort the array by dates
  const sortedDatas = baseData
    .map((data) => Object.assign({}, data, {filename: moment(data.filename, "DD_MM_YYYY")}))
    .sort((a, b) => a.filename.unix() - b.filename.unix());
  // Get all the dates
  const dates = sortedDatas.map(({filename}) => filename);
  // Obtain an array of all person at the good format
  const personWithDates = [].concat(...sortedDatas.map(({filename, data}) =>
    data.map((data) => Object.assign({}, {date: filename.format('DD_MM_YYYY')}, evolveFormat(data)))));
  // Group the person by name
  const groupedPeople = R.groupBy(R.prop('name'), personWithDates);

  // Call formatPerson on every person
  const res = R.compose(
    R.map(formatPerson(dates)),
    R.toPairs,
  )(groupedPeople);
  return {
    dates: dates.map(d => d.format("DD/MM/YYYY")),
    datas: res,
  };
};

/*
 * Transform
 * {prénom,nom,age,'modèle voiture': carModel,'km/ans': kmPerYear}
 * to
 * {name, age, carModel, kmPerYear}
 */
const evolveFormat = ({prénom,nom,age,'modèle voiture': carModel,'km/ans': kmPerYear}) => ({
  name: prénom + ' ' + nom,
  age,
  carModel,
  kmPerYear,
});

/*
 * Functor that first takes a date list of length n and return a function which
 * takes an array of length 2 of form [name, personDefinitions]
 * personDefinitions is an array of all the definitions of the same person found in the different files
 */
const formatPerson = (dates) => ([name, person]) => {
  const keys = R.compose(R.keys, R.omit(['name', 'date']))(person[0]);
  const res = keys.reduce((acc, key) => Object.assign({}, acc, {
    [key]: R.range(0, dates.length).map((index) => {
      const date = dates[index].format("DD_MM_YYYY");
      const accordingPerson = person.find(({date: personDate}) => personDate === date);

      if (!accordingPerson) {
        return null;
      } else {
        return accordingPerson[key];
      }
    }),
  }), {});
  return Object.assign({}, {name: toTitle(name)}, res);
};

module.exports.receiveData = receiveData;
